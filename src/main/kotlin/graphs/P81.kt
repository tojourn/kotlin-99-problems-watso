package graphs

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import graphs.Graph.AdjacencyList
import graphs.Graph.AdjacencyList.Entry
import graphs.Graph.AdjacencyList.Link

fun <V> Graph<V, *>.findAllPaths(from: V, to: V, path: List<V> = emptyList()): List<List<V>> {
    TODO()
}


fun <V> Graph<V, Int>.findShortestPath(from: V, to: V): List<V> {
    TODO()
}


fun <V, L> Graph<V, L>.findShortestPath(from: V, to: V, labelToInt: (L?) -> Int): List<V> {
    TODO()
}

@JvmName("findShortestPathWithNothingLabels")
fun <V> Graph<V, Nothing>.findShortestPath(from: V, to: V): List<V> {
    TODO()
}



class P81Test {
    @Test fun `find paths in undirected graphs`() {
        assertThat("[a]".toGraph().findAllPaths("a", "a"), equalTo(listOf(listOf("a"))))
        assertThat("[a, b]".toGraph().findAllPaths("a", "b"), equalTo(emptyList()))

        assertThat("[a-b]".toGraph().findAllPaths("a", "b"), equalTo(listOf(listOf("a", "b"))))
        assertThat("[a-b]".toGraph().findAllPaths("b", "a"), equalTo(listOf(listOf("b", "a"))))

        assertThat("[a-b, b-c]".toGraph().findAllPaths("a", "c"), equalTo(listOf(listOf("a", "b", "c"))))
        assertThat("[a-b, b-c, a-c]".toGraph().findAllPaths("a", "c"), equalTo(listOf(listOf("a", "b", "c"), listOf("a", "c"))))
        assertThat("[a-b, b-c, a-c]".toGraph().findAllPaths("c", "a"), equalTo(listOf(listOf("c", "b", "a"), listOf("c", "a"))))
    }

    @Test fun `find paths in directed graphs`() {
        assertThat("[a>b]".toGraph().findAllPaths("a", "b"), equalTo(listOf(listOf("a", "b"))))
        assertThat("[a>b, b>c, c>a]".toGraph().findAllPaths("a", "c"), equalTo(listOf(listOf("a", "b", "c"))))

        assertThat("[p>q/9, m>q/7, k, p>m/5]".toLabeledGraph().findAllPaths("p", "q"), equalTo(listOf(listOf("p", "q"), listOf("p", "m", "q"))))
        assertThat("[p>q/9, m>q/7, k, p>m/5]".toLabeledGraph().findAllPaths("p", "k"), equalTo(listOf()))
    }

    @Test fun `find shortest path between nodes`() {
        assertThat("[a]".toGraph().findShortestPath("a", "a"), equalTo(listOf("a")))
        assertThat("[a, b]".toGraph().findShortestPath("a", "b"), equalTo(emptyList()))

        "[a-b/1, b-c/1, a-c/1]".toLabeledGraph().let {
            assertThat(it.findShortestPath("a", "a"), equalTo(listOf("a")))
            assertThat(it.findShortestPath("a", "b"), equalTo(listOf("a", "b")))
            assertThat(it.findShortestPath("a", "c"), equalTo(listOf("a", "c")))
        }
        "[a-b/1, b-c/1, a-c/3]".toLabeledGraph().let {
            assertThat(it.findShortestPath("a", "b"), equalTo(listOf("a", "b")))
            assertThat(it.findShortestPath("a", "c"), equalTo(listOf("a", "b", "c")))
        }

        "[a-b/1, a-c/1, b-e/1, c-d/1, d-e/1]".toLabeledGraph().let {
            assertThat(it.findShortestPath("a", "d"), equalTo(listOf("a", "c", "d")))
            assertThat(it.findShortestPath("a", "e"), equalTo(listOf("a", "b", "e")))
        }
        "[a-b/1, a-c/5, b-e/10, c-d/1, d-e/1]".toLabeledGraph().let {
            assertThat(it.findShortestPath("a", "d"), equalTo(listOf("a", "c", "d")))
            assertThat(it.findShortestPath("a", "e"), equalTo(listOf("a", "c", "d", "e")))
        }
    }
}
