package graphs

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import common.containsAll
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

fun <V1, V2> Graph<V1, *>.isIsomorphicTo(graph: Graph<V2, *>): Boolean {
    TODO()
}


fun <V1, V2> Graph<V1, *>.isomorphicMappingTo(graph: Graph<V2, *>): List<Pair<V1, V2>>? {
    TODO()
}


class P85Test {
    @Test
    fun `graph isomorphism`() {
        assertThat("[a]".toGraph().isomorphicMappingTo("[1]".toGraph())!!, equalTo(listOf(Pair("a", "1"))))

        assertThat(
            "[a-b]".toGraph().isomorphicMappingTo("[1-2]".toGraph())!!, containsAll(
                listOf(
                    Pair("a", "1"),
                    Pair("b", "2")
                )
            )
        )
        assertFalse("[a-b]".toGraph().isIsomorphicTo("[1]".toGraph()))

        assertThat(
            "[a-b, b-c]".toGraph().isomorphicMappingTo("[1-2, 2-3]".toGraph())!!, containsAll(
                listOf(
                    Pair("a", "1"),
                    Pair("b", "2"),
                    Pair("c", "3")
                )
            )
        )
        assertThat(
            "[a-b, b-c]".toGraph().isomorphicMappingTo("[1-2, 1-3]".toGraph())!!, containsAll(
                listOf(
                    Pair("a", "2"),
                    Pair("b", "1"),
                    Pair("c", "3")
                )
            )
        )
        assertFalse("[a-b, b-c]".toGraph().isIsomorphicTo("[1-2, 3]".toGraph()))

        assertThat(
            "[a-b, b-c, c-d, d-a]".toGraph().isomorphicMappingTo("[1-2, 2-3, 3-4, 4-1]".toGraph())!!, containsAll(
                listOf(
                    Pair("a", "1"),
                    Pair("b", "2"),
                    Pair("c", "3"),
                    Pair("d", "4")
                )
            )
        )
        assertTrue("[a-b, b-c, c-d, d-a]".toGraph().isIsomorphicTo("[1-2, 2-3, 3-4, 4-1]".toGraph()))
        assertFalse("[a-b, b-c, c-d, d-a]".toGraph().isIsomorphicTo("[1-2, 2-3, 3-4, 4-2]".toGraph()))
    }
}
