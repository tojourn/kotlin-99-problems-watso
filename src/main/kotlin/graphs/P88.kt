package graphs

import com.natpryce.hamkrest.assertion.assertThat
import org.junit.jupiter.api.Test
import common.containsAll

fun <V, L> Graph<V, L>.components(): List<Graph<V, L>> {
    TODO()
}


class P88Test {
    @Test fun `find connected components of a graph`() {
        assertThat("[a-b]".toGraph().components(), containsAll(listOf(
            "[a-b]".toGraph()
        )))
        assertThat("[a-b, c-d]".toGraph().components(), containsAll(listOf(
            "[a-b]".toGraph(),
            "[c-d]".toGraph()
        )))
    }
}
