package binarytrees

import com.natpryce.hamkrest.assertion.assertThat
import org.junit.jupiter.api.Test
import binarytrees.P55Test.Companion.nodeList
import binarytrees.Tree.End
import binarytrees.Tree.Node
import common.containsAll

fun <T> symmetricBalancedTrees(treeSize: Int, value: T): List<Tree<T>> { TODO() }

class P58Test {
    @Test fun `construct all balanced and symmetric trees`() {
        assertThat(symmetricBalancedTrees(5, "x"), containsAll(nodeList(
            Node("x",
                 Node("x", End, Node("x")),
                 Node("x", Node("x"), End)),
            Node("x",
                 Node("x", Node("x"), End),
                 Node("x", End, Node("x")))
        )))
    }
}
