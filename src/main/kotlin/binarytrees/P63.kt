package binarytrees

import com.natpryce.hamkrest.assertion.assertThat
import org.junit.jupiter.api.Test
import binarytrees.P57Test.Companion.equalToTree
import binarytrees.Tree.End
import binarytrees.Tree.Node

fun <T> completeBinaryTree(nodeAmount: Int, value: T): Tree<T> { TODO() }

class P63Test {
    @Test fun `construct complete binary tree`() {
        assertThat(completeBinaryTree(1, "x"), equalToTree(
            Node("x")
        ))
        assertThat(completeBinaryTree(2, "x"), equalToTree(
            Node("x", Node("x"))
        ))
        assertThat(completeBinaryTree(3, "x"), equalToTree(
            Node("x", Node("x"), Node("x"))
        ))
        assertThat(completeBinaryTree(6, "x"), equalToTree(
            Node("x",
                 Node("x", Node("x"), Node("x")),
                 Node("x", Node("x"), End))
        ))
    }
}
