package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import java.util.NoSuchElementException

fun <T> nth(n: Int, list: List<T>): T = TODO()

class P03Test {

    val sampleList = listOf(1, 1, 2, 3, 5, 8)

    @Test
    fun `find the Nth element of a list`() {
        assertThat(nth(2, sampleList), equalTo(2))
    }

    @Test
    fun `Nth element outside of list size returns null`() {
        assertAll(
            { assertNull(nth(100, sampleList)) },
            { assertNull(nth(6, sampleList)) }
        )

    }
}
