package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

fun <T> insertAt(value: T, n: Int, list: List<T>): List<T> = TODO()

class P21Test {
    @Test
    fun `insert an element at a given position into a list`() {
        assertThat(insertAt('X', 1, "abcd".toList()), equalTo("aXbcd".toList()))
    }
}
