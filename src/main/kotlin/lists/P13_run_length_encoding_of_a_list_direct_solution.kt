package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

fun <T> encodeDirect(list: List<T>): List<Pair<Int, T>> = TODO()

class P13Test {
    @Test
    fun `run-length encoding of a list (direct solution)`() {
        assertThat(encodeDirect("aaaabccaadeeee".toList()), equalTo(listOf(
                Pair(4, 'a'), Pair(1, 'b'), Pair(2, 'c'), Pair(2, 'a'), Pair(1, 'd'), Pair(4, 'e')
        )))
    }
}
