package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

fun <T> duplicate(list: List<T>): List<T> = TODO()

class P14Test {
    @Test
    fun `duplicate the elements of a list`() {
        assertThat(duplicate("abccd".toList()), equalTo("aabbccccdd".toList()))
    }
}

