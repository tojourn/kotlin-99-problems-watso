package misc

import com.natpryce.hamkrest.anyElement
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import common.containsAll
import common.toSeq

fun knightsToursLazy(boardSize: Int, point: Point, tour: Tour = Tour(point)): Sequence<Tour> {
    TODO()
}


fun knightsTours(boardSize: Int, point: Point, tour: Tour = Tour(point)): List<Tour> {
    TODO()
}


data class Point(private val x: Int, private val y: Int) {
    fun knightMoves(boardSize: Int): List<Point> {
        TODO()
    }


    operator fun plus(other: Point): Point {
        TODO()
    }

    operator fun unaryMinus(): Point {
        TODO()
    }

    override fun toString() = "($x,$y)"


}

data class Tour(val points: Collection<Point>) {
    constructor (vararg points: Point) : this(points.asList())

    val size: Int get() = points.size
    fun contains(point: Point) = points.contains(point)
    infix operator fun plus(point: Point) = Tour(points + point)
}

fun Tour.isClosed(boardSize: Int): Boolean {
    return if (size <= 1) false
    else points.last().knightMoves(boardSize).contains(points.first())
}

private fun Int.within(from: Int, until: Int) = this >= from && this < until


class P91Test {
    @Test
    fun `knight's tours`() {
        assertThat(knightsTours(1, Point(0, 0)), equalTo(listOf(Tour(Point(0, 0)))))
        assertThat(knightsTours(2, Point(0, 0)), equalTo(emptyList()))
        assertThat(knightsTours(3, Point(0, 0)), equalTo(emptyList()))

        knightsTours(5, Point(0, 0)).let {
            assertThat(it.size, equalTo(304))
            assertThat(it.filter { it.isClosed(5) }.size, equalTo(0))
            assertThat(
                it, anyElement(
                    equalTo(
                        Tour(
                            Point(0, 0), Point(2, 1), Point(4, 0), Point(3, 2), Point(1, 1),
                            Point(3, 0), Point(4, 2), Point(3, 4), Point(1, 3), Point(0, 1),
                            Point(2, 0), Point(4, 1), Point(2, 2), Point(0, 3), Point(2, 4),
                            Point(4, 3), Point(3, 1), Point(1, 0), Point(0, 2), Point(1, 4),
                            Point(3, 3), Point(1, 2), Point(0, 4), Point(2, 3), Point(4, 4)
                        )
                    )
                )
            )
            assertThat(it, containsAll(knightsToursLazy(5, Point(0, 0)).toList()))
        }
    }

    @Test
    fun `knight's tours for larger boards`() {
        val closedTour = knightsToursLazy(6, Point(0, 0)).find { it.isClosed(6) }!!
        assertThat(
            closedTour.toString(), equalTo(
                "Tour(points=[(0,0), (2,1), (4,0), (3,2), (5,1), (3,0), (1,1), (0,3), (2,2), (0,1), (2,0), (4,1), (5,3), " +
                        "(4,5), (2,4), (0,5), (1,3), (2,5), (4,4), (5,2), (3,3), (1,4), (0,2), (1,0), (3,1), (5,0), (4,2), " +
                        "(5,4), (3,5), (4,3), (5,5), (3,4), (1,5), (2,3), (0,4), (1,2)])"
            )
        )
    }
}
