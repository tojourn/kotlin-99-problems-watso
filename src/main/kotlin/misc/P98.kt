package misc

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo

import org.junit.jupiter.api.Test
import common.fill
import common.tail
import common.toSeq
import misc.Nonogram.Box
import misc.Nonogram.Companion.parse
import misc.Nonogram.Constraint
import org.junit.jupiter.api.Disabled
import java.util.*

@Suppress("unused") // Because this class is a "namespace".
class Nonogram {

    data class Board(
        private val width: Int,
        private val height: Int,
        private val rowConstrains: List<Constraint>,
        private val columnConstraints: List<Constraint>,
        private val cells: List<ArrayList<Boolean>> = 1.rangeTo(height).map { ArrayList<Boolean>().fill(width, false) }
    ) {

        fun solve(rowIndex: Int = 0): Sequence<Board>{
            TODO()
        }

        private fun hasContradiction(): Boolean {
            TODO()
        }

        fun columnBoxHeights(column: Int): List<Int> {
            TODO()
        }

        private fun apply(boxes: List<Box>, rowIndex: Int): Board {
            TODO()
        }

        private fun copy(): Board = Board(width, height, rowConstrains, columnConstraints, cells.map { ArrayList(it) })

        override fun toString(): String {
            TODO()
        }
    }

    data class Box(val index: Int, val width: Int)

    data class Constraint(val boxes: List<Int>) {
        constructor(vararg boxes: Int): this(boxes.toList())

        fun possibleBoxes(width: Int, startIndex: Int = 0): Sequence<List<Box>> {
            TODO()
        }
    }

    companion object {
        fun String.parse(): Board {
            TODO()
        }
    }
}

class P98Test {

    @Test fun `all possibles boxes within constraint`() {
        Constraint(3).apply {
            assertThat(this.possibleBoxes(width = 2).toList(), equalTo(emptyList()))
            assertThat(this.possibleBoxes(width = 3).toList(), equalTo(listOf(
                listOf(Box(0, 3))
            )))
            assertThat(this.possibleBoxes(width = 5).toList(), equalTo(listOf(
                listOf(Box(0, 3)), listOf(Box(1, 3)), listOf(Box(2, 3))
            )))
        }

        Constraint(2, 1).apply {
            assertThat(this.possibleBoxes(width = 4).toList(), equalTo(listOf(
                listOf(Box(0, 2), Box(3, 1))
            )))
            assertThat(this.possibleBoxes(width = 6).toList(), equalTo(listOf(
                listOf(Box(0, 2), Box(3, 1)),
                listOf(Box(0, 2), Box(4, 1)),
                listOf(Box(0, 2), Box(5, 1)),
                listOf(Box(1, 2), Box(4, 1)),
                listOf(Box(1, 2), Box(5, 1)),
                listOf(Box(2, 2), Box(5, 1))
            )))
        }

        Constraint(3, 2).apply {
            assertThat(this.possibleBoxes(width = 7).toList(), equalTo(listOf(
                listOf(Box(0, 3), Box(4, 2)),
                listOf(Box(0, 3), Box(5, 2)),
                listOf(Box(1, 3), Box(5, 2))
            )))
        }
    }

    @Test fun `counting column boxes heights`() {
        val nonogram = """
            *|_|X|X|X|_|_|_|_| 3
            *|X|X|_|X|_|_|_|_| 2 1
            *|_|X|X|X|_|_|X|X| 3 2
            *|_|_|X|X|_|_|X|X| 2 2
            *|_|_|X|X|X|X|X|X| 6
            *|X|_|X|X|X|X|X|_| 1 5
            *|X|X|X|X|X|X|_|_| 6
            *|_|_|_|_|X|_|_|_| 1
            *|_|_|_|X|X|_|_|_| 2
            * 1 3 1 7 5 3 4 3
            * 2 1 5 1
        """.trimMargin("*").parse()

        assertThat(nonogram.columnBoxHeights(0), equalTo(listOf(1, 2)))
        assertThat(nonogram.columnBoxHeights(1), equalTo(listOf(3, 1)))
        assertThat(nonogram.columnBoxHeights(2), equalTo(listOf(1, 5)))
        assertThat(nonogram.columnBoxHeights(3), equalTo(listOf(7, 1)))
    }

    @Test fun `parse and convert back to string`() {
        val s = """
            *|_|X|X|X|_|_|_|_| 3
            *|X|X|_|X|_|_|_|_| 2 1
            *|_|X|X|X|_|_|X|X| 3 2
            *|_|_|X|X|_|_|X|X| 2 2
            *|_|_|X|X|X|X|X|X| 6
            *|X|_|X|X|X|X|X|_| 1 5
            *|X|X|X|X|X|X|_|_| 6
            *|_|_|_|_|X|_|_|_| 1
            *|_|_|_|X|X|_|_|_| 2
            * 1 3 1 7 5 3 4 3
            * 2 1 5 1
        """
        val nonogram = s.trimMargin("*").parse()

        assertThat(nonogram.toString(), equalTo(s.trimMargin("*")))
    }

    @Test fun `solve nonogram from readme`() {
        val nonogram = """
            *|_|_|_|_|_|_|_|_| 3
            *|_|_|_|_|_|_|_|_| 2 1
            *|_|_|_|_|_|_|_|_| 3 2
            *|_|_|_|_|_|_|_|_| 2 2
            *|_|_|_|_|_|_|_|_| 6
            *|_|_|_|_|_|_|_|_| 1 5
            *|_|_|_|_|_|_|_|_| 6
            *|_|_|_|_|_|_|_|_| 1
            *|_|_|_|_|_|_|_|_| 2
            * 1 3 1 7 5 3 4 3
            * 2 1 5 1
        """.trimMargin("*").parse()

        assertThat(nonogram.solve().first(), equalTo("""
            *|_|X|X|X|_|_|_|_| 3
            *|X|X|_|X|_|_|_|_| 2 1
            *|_|X|X|X|_|_|X|X| 3 2
            *|_|_|X|X|_|_|X|X| 2 2
            *|_|_|X|X|X|X|X|X| 6
            *|X|_|X|X|X|X|X|_| 1 5
            *|X|X|X|X|X|X|_|_| 6
            *|_|_|_|_|X|_|_|_| 1
            *|_|_|_|X|X|_|_|_| 2
            * 1 3 1 7 5 3 4 3
            * 2 1 5 1
        """.trimMargin("*").parse()
        ))
    }

    @Disabled // because it's too slow for CI (~ 2 minutes 30 seconds)
    @Test fun `solve GCHQ Christmas Puzzle`() {
        // See http://makercasts.org/articles/gchq-christmas-puzzle
        val nonogram = """
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 7 3 1 1 7
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 1 2 2 1 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 3 1 3 1 1 3 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 3 1 1 6 1 3 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 3 1 5 2 1 3 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 1 2 1 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 7 1 1 1 1 1 7
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 3 3
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 2 3 1 1 3 1 1 2
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 1 3 2 1 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 4 1 4 2 1 2
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 1 1 1 1 4 1 3
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 2 1 1 1 2 5
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 3 2 2 6 3 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 9 1 1 2 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 2 1 2 2 3 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 3 1 1 1 1 5 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 2 2 5
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 7 1 2 1 1 1 3
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 1 2 1 2 2 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 3 1 4 5 1
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 3 1 3 10 2
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 3 1 1 6 6
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 1 1 2 1 1 2
            *|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| 7 2 1 2 5
            * 7 1 1 1 1 1 7 1 2 2 1 1 4 3 1 2 1 6 7 1 1 1 1 1 7
            * 2 1 3 3 3 1 1 1 1 2 7 2 1 3 2 2 3 2 1 1 3 3 3 1 1
            * 1 2 1 1 1 1 1 3 2 1 3 3 1 1 5 1 3 1 4 1 1 1 1 2 3
            * 1 2 3 1 1 2 1   1 2 2 1 2 1 2 1 2   1 1 3 1 4 2 2
            * 7 1 1 5 4 1 1   8 1 1 1 6 1 2 1 1   1 4 7 1 3 2 1
            *   1 3 1 1 1 1   2 1   1   3   1 8   3   1 2 3 6 1
            *     1 3 3   7   1 1   1   1   1 1         1   1
            *     3 1 1         2   1       2           1
            *     1                         2           4
        """.trimMargin("*").parse()

        assertThat(nonogram.solve().first(), equalTo("""
            *|X|X|X|X|X|X|X|_|X|X|X|_|_|_|X|_|X|_|X|X|X|X|X|X|X| 7 3 1 1 7
            *|X|_|_|_|_|_|X|_|X|X|_|X|X|_|_|_|_|_|X|_|_|_|_|_|X| 1 1 2 2 1 1
            *|X|_|X|X|X|_|X|_|_|_|_|_|X|X|X|_|X|_|X|_|X|X|X|_|X| 1 3 1 3 1 1 3 1
            *|X|_|X|X|X|_|X|_|X|_|_|X|X|X|X|X|X|_|X|_|X|X|X|_|X| 1 3 1 1 6 1 3 1
            *|X|_|X|X|X|_|X|_|_|X|X|X|X|X|_|X|X|_|X|_|X|X|X|_|X| 1 3 1 5 2 1 3 1
            *|X|_|_|_|_|_|X|_|_|X|X|_|_|_|_|_|_|_|X|_|_|_|_|_|X| 1 1 2 1 1
            *|X|X|X|X|X|X|X|_|X|_|X|_|X|_|X|_|X|_|X|X|X|X|X|X|X| 7 1 1 1 1 1 7
            *|_|_|_|_|_|_|_|_|X|X|X|_|_|_|X|X|X|_|_|_|_|_|_|_|_| 3 3
            *|X|_|X|X|_|X|X|X|_|_|X|_|X|_|X|X|X|_|X|_|_|X|_|X|X| 1 2 3 1 1 3 1 1 2
            *|X|_|X|_|_|_|_|_|_|X|X|X|_|X|X|_|_|_|_|X|_|_|_|X|_| 1 1 3 2 1 1
            *|_|X|X|X|X|_|X|_|X|X|X|X|_|X|X|_|X|_|_|_|_|X|X|_|_| 4 1 4 2 1 2
            *|_|X|_|X|_|_|_|X|_|_|_|X|_|X|_|X|X|X|X|_|X|_|X|X|X| 1 1 1 1 1 4 1 3
            *|_|_|X|X|_|_|X|_|X|_|X|_|_|_|_|_|_|X|X|_|X|X|X|X|X| 2 1 1 1 2 5
            *|_|_|_|X|X|X|_|X|X|_|X|X|_|X|X|X|X|X|X|_|X|X|X|_|X| 3 2 2 6 3 1
            *|X|_|X|X|X|X|X|X|X|X|X|_|X|_|X|_|_|X|X|_|_|_|_|X|_| 1 9 1 1 2 1
            *|_|X|X|_|X|_|_|X|X|_|_|X|X|_|_|X|X|X|_|_|_|_|_|X|_| 2 1 2 2 3 1
            *|X|X|X|_|X|_|X|_|X|_|_|_|_|X|_|_|X|X|X|X|X|_|X|_|_| 3 1 1 1 1 5 1
            *|_|_|_|_|_|_|_|_|X|_|_|X|X|_|_|X|X|_|_|_|X|X|X|X|X| 1 2 2 5
            *|X|X|X|X|X|X|X|_|X|_|_|_|X|X|_|_|X|_|X|_|X|_|X|X|X| 7 1 2 1 1 1 3
            *|X|_|_|_|_|_|X|_|X|X|_|_|X|_|_|X|X|_|_|_|X|X|_|X|_| 1 1 2 1 2 2 1
            *|X|_|X|X|X|_|X|_|_|_|X|X|X|X|_|_|X|X|X|X|X|_|_|X|_| 1 3 1 4 5 1
            *|X|_|X|X|X|_|X|_|X|X|X|_|X|X|X|X|X|X|X|X|X|X|_|X|X| 1 3 1 3 10 2
            *|X|_|X|X|X|_|X|_|X|_|_|X|X|X|X|X|X|_|X|X|X|X|X|X|_| 1 3 1 1 6 6
            *|X|_|_|_|_|_|X|_|_|X|X|_|_|_|_|_|_|X|_|X|_|X|X|_|_| 1 1 2 1 1 2
            *|X|X|X|X|X|X|X|_|X|X|_|_|_|X|_|X|X|_|_|_|X|X|X|X|X| 7 2 1 2 5
            * 7 1 1 1 1 1 7 1 2 2 1 1 4 3 1 2 1 6 7 1 1 1 1 1 7
            * 2 1 3 3 3 1 1 1 1 2 7 2 1 3 2 2 3 2 1 1 3 3 3 1 1
            * 1 2 1 1 1 1 1 3 2 1 3 3 1 1 5 1 3 1 4 1 1 1 1 2 3
            * 1 2 3 1 1 2 1   1 2 2 1 2 1 2 1 2   1 1 3 1 4 2 2
            * 7 1 1 5 4 1 1   8 1 1 1 6 1 2 1 1   1 4 7 1 3 2 1
            * 1 3 1 1 1 1   2 1   1   3   1 8   3   1 2 3 6 1
            * 1 3 3   7   1 1   1   1   1 1         1   1
            * 3 1 1         2   1       2           1
            * 1                         2           4
        """.trimMargin("*").parse()
        ))
    }
}
