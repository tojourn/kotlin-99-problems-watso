package misc

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo

import org.junit.jupiter.api.Test
import common.tail
import graphs.Graph
import graphs.Graph.TermForm
import graphs.Graph.TermForm.Term
import graphs.isIsomorphicTo
import graphs.toGraph
import lists.combinations
import org.junit.jupiter.api.Assertions.assertTrue


fun findAllRegularGraphs(degree: Int, nodeAmount: Int): List<Graph<Int, Nothing>> {
    TODO()
}


private fun edgeCombinations(nodes: List<Int>, degree: Int, result: List<Pair<Int, Int>> = emptyList()): List<List<Pair<Int, Int>>> {
    TODO()
}

private fun <V> List<Pair<V, V>>.toGraph(): Graph<V, Nothing> {
    TODO()
}
private fun <V1, V2, L> Graph<V1, L>.mapValue(f: (V1) -> V2): Graph<V2, L> {
    TODO()
}

private fun Graph<*, *>.isRegular(degree: Int): Boolean {
    TODO()
}


class P94Test {
    @Test fun `regular graphs for 2 nodes`() {
        assertThat(findAllRegularGraphs(degree = 1, nodeAmount = 2), equalTo(intGraphs("[0-1]")))
        assertThat(findAllRegularGraphs(degree = 2, nodeAmount = 2), equalTo(emptyList()))
    }

    @Test fun `regular graphs for 3 nodes`() {
        assertThat(findAllRegularGraphs(degree = 1, nodeAmount = 3), equalTo(emptyList()))
        assertThat(findAllRegularGraphs(degree = 2, nodeAmount = 3), equalTo(intGraphs("[1-2, 0-2, 0-1]")))
    }

    @Test fun `regular graphs for 4 nodes`() {
        assertThat(findAllRegularGraphs(degree = 2, nodeAmount = 4), equalTo(intGraphs(
            "[2-3, 1-3, 0-2, 0-1]"
        )))
        assertThat(findAllRegularGraphs(degree = 3, nodeAmount = 4), equalTo(intGraphs(
            "[2-3, 1-3, 1-2, 0-3, 0-2, 0-1]"
        )))
    }

    @Test fun `non-isomorphic 3-regular graphs with 6 nodes`() {
        assertThat(findAllRegularGraphs(degree = 3, nodeAmount = 6), equalTo(intGraphs(
            "[4-5, 3-5, 3-4, 2-5, 1-4, 1-2, 0-3, 0-2, 0-1]",
            "[3-5, 3-4, 2-5, 2-4, 1-5, 1-4, 0-3, 0-2, 0-1]"
        )))
    }

    @Test fun `produced graphs are regular`() {
        findAllRegularGraphs(degree = 3, nodeAmount = 6).forEach { it ->
            assertTrue(it.isRegular(degree = 3))
        }
    }

    private fun intGraphs(vararg graphs: String): List<Graph<Int, Nothing>> =
        graphs.map { it.toGraph().mapValue(String::toInt) }
}
