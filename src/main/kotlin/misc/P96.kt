package misc

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import misc.GameOfLife.Companion.Board
import misc.GameOfLife.Companion.Cell
import org.junit.jupiter.api.Assertions.assertFalse

@Suppress("unused")
class GameOfLife {
    companion object {

        data class Cell(val x: Int, val y: Int) {
            fun neighbours(): List<Cell> {
                TODO()
            }
            override fun toString() = "($x,$y)"
        }

        data class Board(val cells: Set<Cell>) {
            constructor(vararg cells: Cell): this(cells.toSet())

            fun evolve(): Board {
                TODO()
            }

            private fun liveNeighboursOf(cell: Cell): Int{
                TODO()
            }
        }
    }
}

class P96Test {
    @Test fun `lonely cells die`() {
        assertThat(Board(Cell(0, 0)).evolve(), equalTo(Board()))
        assertThat(Board(Cell(0, 0), Cell(0, 1)).evolve(), equalTo(Board()))
        assertThat(Board(Cell(0, 0), Cell(2, 2)).evolve(), equalTo(Board()))
    }

    @Test fun `cells with enough neighbours stay alive`() {
        assertTrue(parseBoard(
            "**-",
            "*--"
        ).evolve().cells.containsAll(listOf(
            Cell(0, 0), Cell(1, 0), Cell(0, 1)
        )))
        assertTrue(parseBoard(
            "**-",
            "**-"
        ).evolve().cells.containsAll(listOf(
            Cell(0, 0), Cell(0, 1), Cell(1, 0), Cell(1, 1)
        )))
    }

    @Test fun `overpopulated cells die`() {
        assertFalse(parseBoard(
            "**-",
            "**-",
            "*--"
        ).evolve().cells.containsAll(listOf(
            Cell(0, 1), Cell(1, 1)
        )))
    }

    @Test fun `dead cell with three neighbours becomes alive`() {
        assertTrue(parseBoard(
            "**",
            "*-"
        ).evolve().cells.containsAll(listOf(
            Cell(1, 1)
        )))
    }

    @Test fun `block pattern never changes`() {
        assertThat(
            parseBoard(
                "**-",
                "**-"
            ).evolve(), equalTo(
            parseBoard(
                "**-",
                "**-"
            )
        ))
    }

    @Test fun `beehive pattern never changes`() {
        assertThat(
            parseBoard(
                "-**-",
                "*--*",
                "-**-"
            ).evolve(), equalTo(
            parseBoard(
                "-**-",
                "*--*",
                "-**-"
            )
        ))
    }

    @Test fun `blinker pattern oscillates`() {
        assertThat(
            parseBoard(
                "-*-",
                "-*-",
                "-*-"
            ).evolve(), equalTo(
            parseBoard(
                "---",
                "***",
                "---"
            )
        ))
        assertThat(
            parseBoard(
                "---",
                "***",
                "---"
            ).evolve(), equalTo(
            parseBoard(
                "-*-",
                "-*-",
                "-*-"
            )
        ))
    }

    @Test fun `toad pattern oscillates`() {
        assertThat(
            parseBoard(
                "----",
                "-***",
                "***-",
                "----"
            ).evolve(), equalTo(
            parseBoard(
                "--*-",
                "*--*",
                "*--*",
                "-*--"
            )
        ))
        assertThat(
            parseBoard(
                "--*-",
                "*--*",
                "*--*",
                "-*--"
            ).evolve(), equalTo(
            parseBoard(
                "----",
                "-***",
                "***-",
                "----"
            )
        ))
    }

    @Test fun `convert board to string`() {
        assertThat(Board(Cell(0, 0), Cell(2, 0), Cell(1, 1), Cell(0, 2)).toPrettyString(), equalTo(
            "*-*\n" +
            "-*-\n" +
            "*--"
        ))
    }

    @Test fun `convert string to board`() {
        val board = parseBoard(
            "*-*\n" +
            "-*-\n" +
            "*--"
        )
        assertThat(board, equalTo(
            Board(Cell(0, 0), Cell(2, 0), Cell(1, 1), Cell(0, 2))
        ))
    }

    private fun parseBoard(s: String): Board = parseBoard(*s.split(Regex("\n")).toTypedArray())

    private fun parseBoard(vararg lines: String): Board {
        val cells = lines.mapIndexed { y, line ->
            line.toCharArray().mapIndexed { x, c ->
                if (c == '*') Cell(x, y) else null
            }
        }
        return Board(cells.flatten().filterNotNull().toSet())
    }

    private fun Board.toPrettyString(): String {
        val xMax = cells.map { it.x }.max()!!
        val yMax = cells.map { it.y }.max()!!

        return 0.rangeTo(yMax).joinToString("\n") { y ->
            0.rangeTo(xMax).joinToString("") { x ->
                if (cells.any { it.x == x && it.y == y }) "*" else "-"
            }
        }
    }

}
