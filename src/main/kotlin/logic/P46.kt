@file:Suppress("NOTHING_TO_INLINE", "unused")

package logic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

inline fun Boolean.not_() = !this
inline fun Boolean.and_(other: Boolean) = this && other
inline fun Boolean.or_(other: Boolean) = this || other
inline fun Boolean.nand_(other: Boolean) = this.and_(other).not_()
inline fun Boolean.nor_(other: Boolean) = this.or_(other).not_()
inline fun Boolean.xor_(other: Boolean) = this.xor(other)
inline fun Boolean.equ_(other: Boolean) = this.xor_(other).not_()
inline fun Boolean.impl_(other: Boolean) = this.or_(other.not_())

fun truthTable(f: (Boolean, Boolean) -> Boolean): List<Row> =
   TODO()

fun printTruthTable(f: (Boolean, Boolean) -> Boolean) {
    TODO()
}

data class Row(val a: Boolean, val b: Boolean, val result: Boolean)


class P46Test {
    @Test fun `logical expressions`() {
        assertThat(true.and_(true), equalTo(true))
        assertThat(true.xor_(true), equalTo(false))
    }

    @Test fun `truth tables for logical expressions`() {
        assertThat(truthTable(Boolean::and_), equalTo(listOf(
            Row(true, true, true),
            Row(true, false, false),
            Row(false, true, false),
            Row(false, false, false)
        )))
        assertThat(truthTable(Boolean::xor_), equalTo(listOf(
            Row(true, true, false),
            Row(true, false, true),
            Row(false, true, true),
            Row(false, false, false)
        )))
        assertThat(truthTable(Boolean::equ_), equalTo(listOf(
            Row(true, true, true),
            Row(true, false, false),
            Row(false, true, false),
            Row(false, false, true)
        )))

        printTruthTable { a, b -> a.and_(a.or_(b.not_())) }
        assertThat(truthTable { a, b -> a.and_(a.or_(b.not_())) }, equalTo(listOf(
            Row(true, true, true),
            Row(true, false, true),
            Row(false, true, false),
            Row(false, false, false)
        )))
    }
}
