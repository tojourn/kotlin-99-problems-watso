package logic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import common.tail


fun String.createEncoding(): HuffmanEncoding { TODO() }

fun String.encode(encoding: HuffmanEncoding): String { TODO() }

fun String.decode(encoding: HuffmanEncoding): String { TODO() }
fun String.charAndFrequency(): Map<Char, Int> { TODO() }

fun createEncoding(frequencyByChar: Map<Char, Int>): HuffmanEncoding { TODO() }

fun buildTree(nodes: List<Node>): Node { TODO() }

data class HuffmanEncoding(val tree: Node, val codeByChar: Map<Char, String>)


data class Node(val weight: Int, val char: Char?, val left: Node? = null, val right: Node? = null) {
    fun leavesWithPath(): List<Pair<Char, String>> { TODO() }

    fun followCode(code: Char): Node? { TODO() }

    override fun toString(): String { TODO() }
}


class P50Test {
    @Test fun `letter to code mapping`() {
        assertThat(
            createEncoding(linkedMapOf(
                Pair('a', 25), Pair('b', 21), Pair('c', 18), Pair('d', 14), Pair('e', 9), Pair('f', 7), Pair('g', 6)
            )).codeByChar,
            equalTo<Map<Char, String>>(linkedMapOf(
                Pair('a', "10"), Pair('b', "00"), Pair('c', "111"), Pair('d', "110"), Pair('e', "010"), Pair('f', "0111"), Pair('g', "0110")
            ))
        )
    }

    @Test fun `encoding and decoding a string`() {
        val string = "this is a sentence"
        val encoding = string.createEncoding()

        val encodedString: String = string.encode(encoding)
        assertThat(encodedString, equalTo("00110000101011100101011101001110101111011001111011000111"))

        val decodedString = encodedString.decode(encoding)
        assertThat(decodedString, equalTo("this is a sentence"))
    }
}
