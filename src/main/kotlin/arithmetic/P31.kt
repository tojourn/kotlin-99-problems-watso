package arithmetic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

fun Int.isPrime(): Boolean { TODO() }

class P31Test {
    @Test fun `determine whether a given integer number is prime`() {
        assertThat((0..13).filter { it.isPrime() }, equalTo(
            listOf(2, 3, 5, 7, 11, 13))
        )
    }
}
