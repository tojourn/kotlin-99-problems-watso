package arithmetic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

fun Int.goldbach(): Pair<Int, Int> { TODO() }

class P40Test {
    @Test fun `Goldbach's conjecture`() {
        assertThat(4.goldbach(), equalTo(Pair(2, 2)))
        assertThat(28.goldbach(), equalTo(Pair(5, 23)))
    }
}
